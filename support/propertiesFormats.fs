// Esempio di una funzione base
let prop_<nome_funzione> <parametri> =
  let r1 = <ModuloSoluzione>.<nome_funzione> <parametri>
  let r2 = <ModuloStudente>.<nome_funzione> <parametri>
  r1 = r2

do Check.Quick prop_<nome_funzione>

// Una funzione con un generatore associato 
// <paremtri_out_gen> = parametri generati dal generatore
// <parametri> = <parametri_out_gen> + <parametri_other>
let prop_<nome_funzione> <parametri_other> =
  let check (<parametri_out_gen) =
    let r1 = <ModuloSoluzione>.<nome_funzione> <parametri>
    let r2 = <ModuloStudente>.<nome_funzione> <parametri>
    r1 = r2

  Prop.forAll <ModuloSoluzione>.<nome_generatore> <| fun x -> check x

do Check.Quick prop_<nome_funzione>

// Una funzione con sequenze in input e output
// <parametri_seq> = parametri che sono delle sequenze
// <parametri_other> = parametri che non sono delle sequenze
// <parametri> = <parametri_seq> + <parametri_other>
// Ogni parametro in <parametri_seq> viene sostituito con un parametro <_fi>
// quindi i <parametri_seq> diventano <_f0 _f1 ... _fn>
let prop_<nome_funzione> <_f0 _f1 ... _fn> <parametri_other> =
    let s0 = Seq.initInfinite _f0
    let s1 = Seq.initInfinite _f1
    ...
    let sn = Seq.initInfinite _fn

    let r1 = <ModuloSoluzione>.<nome_funzione> <parametri> |> Seq.take 10 |> Seq.toList
    let r2 = <ModuloStudente>.<nome_sequenza> <parametri> |> Seq.take 10 |> Seq.toList
    r1 = r2

do Check.Quick prop_<nome_sequenza>

// Una funzione con sequenze e generatore associato
// <parametri_seq> = parametri che sono delle sequenze
// <parametri_out_gen> parametri generati dal generatore
// <parametri_other> = parametri restanti
// <parametri> = <parametri_seq> + <parametri_out_gen> + <parametri_other>
// Ogni parametro in <parametri_seq> viene sostituito con un parametro <_fi>
// quindi i <parametri_seq> diventano <_f0 _f1 ... _fn>
let prop_<nome_funzione> <_f0 _f1 ... _fn> <parametri_other> =
    let s0 = Seq.initInfinite _f0
    let s1 = Seq.initInfinite _f1
    ...
    let sn = Seq.initInfinite _fn

    let check (<parametri_out_gen>) =
        let r1 = <ModuloSoluzione>.<nome_funzione> <parametri> |> Seq.take 10 |> Seq.toList
        let r2 = <ModuloStudente>.<nome_sequenza> <parametri> |> Seq.take 10 |> Seq.toList
        r1 = r2

    Prop.forAll <ModuloSoluzione>.<nome_generatore> <| fun x -> check x

do Check.Quick prop_<nome_sequenza>