﻿#r "../packages/FsCheck.2.9.2/lib/net452/FsCheck.dll"
open FsCheck
open System


//Semplice funzione che data una lista
//ne calcola la lunghezza
let rec length ls =
    match ls with
        | [] -> 0
        | hd::tl -> 1 + length tl

//La proprieta' da testare, controlla
//che la funzione restituisca lo stesso valore
//del metodo built-in Length
let prop_length ls =
    length ls = ls.Length

//Chiamata della libreria per
//testare la proprieta'
Check.Quick prop_length

//Responso
> Ok, passed 100 tests.
> val it : unit = ()


//Semplice funzione che data una lista
//ne calcola la lunghezza (sbagliata!)
let rec length ls =
    match ls with
        | [] -> 1
        | hd::tl -> 1 + length tl

//La proprieta' da testare, controlla
//che la funzione restituisca lo stesso valore
//del metodo built-in Length
let prop_length ls =
    length ls = ls.Length

//Chiamata della libreria per
//testare la proprieta'
Check.Quick prop_length

//Responso
> Falsifiable, after 1 test (0 shrinks) (StdGen (2110734248,296379989)):
> Original: []
> val it : unit = ()


// downto0 : int -> int list 
let rec downto0 n =
    match n  with
        | 0 -> [0]
        | _ ->  n :: downto0 (n-1) 

let generator_positiveNumber =
    Arb.filter (fun x -> x > 0) Arb.from<int>