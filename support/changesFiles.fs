type binTree<'a> =
   | Null
   | Node of 'a * binTree<'a> * binTree<'a>

let rec serializeToStudent = function
   | Null -> Student.Null
   | Node(irpd, txng, rjzl) -> Student.Node(irpd, serializeToStudent txng, serializeToStudent rjzl)

let rec serializeToProf = function
   | Null -> Prof.Null
   | Node(qdsu, raof, krsk) -> Prof.Node(qdsu, serializeToProf raof, serializeToProf krsk)

let rec compare_ADT (student: Student.binTree<'a>, prof: Prof.binTree<'a>) =
   match student, prof with
      | Student.Null, Prof.Null -> true
      | Student.Node(uxew, xivm, wujb), Prof.Node(gstp, vsjw,nele) -> uxew = gstp && (compare_ADT (xivm, vsjw)) && (compare_ADT (wujb, nele))
      | _, _ -> false



// <parametri_out_gen> = parametri del generatore built-in
// <parametri> = <parametri_other> + <parametri_out_gen>
// <nome_generatore> = nome del generatore built-in
let prop_<nome_funzione> <parametri_other> (<nome_generatore> <parametri_out_gen>) =
   let r1 = <ModuloSoluzione>.<nome_funzione> <parametri>
   let r2 = <ModuloStudente>.<nome_funzione> <parametri>
   r1 = r2
do Check.Quick prop_<nome_funzione>



// <valore> = valore sufficiente piccolo per determinare
// se i due float siano uguali a meno di rappresentazione
// abs = valore assoluto della sottrazione tra i due risultati
let prop_<nome_funzione> <parametri> =
   let r1 = <ModuloSoluzione>.<nome_funzione> <parametri>
   let r2 = <ModuloStudente>.<nome_funzione> <parametri>
   abs (r1 - r2) <= <valore>
do Check.Quick prop_<nome_funzione>



// <parametri_other> = tutti i parametri che non sono tipi di dati algebrici
// tutti i tipi di dati algebrici sono nella forma (parametro: nome_datoAlgebrico<int>),
// nell'esempio e' presente solo uno ma possono essere molteplici
let prop_<nome_funzione> <parametri_other> (parametro: nome_datoAlgebrico<int>) =
   let r1 = <ModuloSoluzione>.<nome_funzione> <parametri_other> (serializeTo<ModuloSoluzione> parametro)
   let r2 = <ModuloStudente>.<nome_funzione> <parametri_other> (serializeToStudent parametro)
   r1 = r2
do Check.Quick prop_<nome_funzione>



// <parametri_other> = tutti i parametri che non sono tipi di dati algebrici
// tutti i tipi di dati algebrici sono nella forma (parametro: nome_datoAlgebrico<int>),
// nell'esempio e' presente solo uno ma possono essere molteplici
// compare_ADT = funzione apposita per confrontare due tipi di dati algebrici
// uguali ma definiti in moduli diversi
let prop_<nome_funzione> <parametri_other> (parametro: nome_datoAlgebrico<int>) =
   let r1 = <ModuloSoluzione>.<nome_funzione> <parametri_other> (serializeTo<ModuloSoluzione> parametro)
   let r2 = <ModuloStudente>.<nome_funzione> <parametri_other> (serializeToStudent parametro)
   compare_ADT (r1, r2)
do Check.Quick prop_<nome_funzione>



// Semplice discriminated union che
// rappresenta un intero o un valore booleano
type IntOBool =
    | I of int
    | B of Bool

// Discriminated union con casi vuoti
type Directory =
    | Root
    | Subdirectory of string

// Discriminated union con tipi generici
type 'a Option =
    | Some of 'a
    | None


// Tipi di dati ricorsivi
type 'a List =
    | Null
    | Cons of 'a * 'a List

// Tipi di dati mutualmente ricorsivi
type 'a Tree =
    | Empty
    | Node of 'a * 'a Forest
and 'a Forest =
    | Null
    | Cons of 'a Tree * 'a Forest