// Caricamento delle DLL
open Prof
open System
open System.Numerics
open ValidatoreLib
open Newtonsoft.Json
open FsCheck
open Student

// Creazione del file di output e della sequenza di supporto
let __outputFile_ = System.IO.File.CreateText("path del file di output")
let ____makeInfiniteSeq____ sq = seq { while true do yield! sq }

(*
    Per ogni funzione viene generata la relativa proposizione
    dal generatore di proposizioni
*)

//Chiusura del file di output
__outputFile_.Close()
__outputFile_.Dispose()
