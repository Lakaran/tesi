\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {italian}
\defcounter {refsection}{0}\relax 
\select@language {italian}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Ringraziamenti}{iv}{chapter*.2}
\defcounter {refsection}{0}\relax 
\contentsline {part}{I\hspace {1em}Idea e funzionamento del programma}{1}{part.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduzione}{2}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}L'idea alla base del progetto}{3}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Come nasce il progetto}{3}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Tecnologie usate}{4}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}QuickCheck}{4}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Il formato Json}{6}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Il funzionamento}{8}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Procedimento generale}{8}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Il file di configurazione}{9}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Estrazione delle informazioni sul modello}{10}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Generazione degli script}{10}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Generazione delle proposizioni}{11}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {part}{II\hspace {1em}Funzionalit\`a aggiunte}{14}{part.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Tipi di dati algebrici}{15}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Introduzione ai tipi in F\#}{15}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Supporto ai tipi di dati algebrici}{17}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Numeri in virgola mobile e Generatori built-in}{21}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Numeri in virgola mobile}{21}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Generatori built-in}{22}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Conclusioni}{25}{chapter*.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliografia}{26}{chapter*.4}
